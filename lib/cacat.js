var request = require('request');
var cheerio = require('cheerio');

module.exports = function Cacat() {
    this.get = (number) => {
        if(!number || typeof number !== "number")
            number = 1;
        
        let promises = [];
        for(let  i = 0; i < number; i ++) {
            promises.push(this.one());
        }

        return Promise.all(promises);
    };

    this.one = () => {
        return new Promise((resolve, reject) => {
            request("http://cacat.ro", function(error, response, html){
                if(!error){
                    let $ = cheerio.load(html);
                    let result = $(".zicere").text();
                    resolve(result);
                } else {
                    reject(error);
                }
            });
        });
    };
};